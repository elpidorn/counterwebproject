package com.qaagility.controller;

public class Controller {

    public int division(int firstNumber, int secondNumber) {
        if (secondNumber == 0) {
            return Integer.MAX_VALUE;
	}
        else{
            return firstNumber / secondNumber;
	}
    }

}
