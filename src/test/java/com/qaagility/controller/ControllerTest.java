package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class ControllerTest {

	@Test
	public void testDivisionZero() throws Exception {
        	
		int result = new Controller().division(8,0);
		assertEquals("firstIf", Integer.MAX_VALUE, result);
	}

        @Test
        public void testDevisionNotZero() throws Exception {

                int result = new Controller().division(8,2);
                assertEquals("SecondIf", 4, result);
        }


}
